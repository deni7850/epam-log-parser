import logging
import os
from logging import getLogger
from pathlib import Path
from typing import Iterable

from models import LogFileStats, timer, InputSettings, convert_str_to_datetime

logger = getLogger('main')


class LogPathParser:
    def __init__(self, arguments, config: InputSettings):
        self.arguments = arguments
        self.path = self.arguments.path
        self.config = config

    def process_logs(self) -> Iterable[LogFileStats]:
        if os.path.isfile(self.path):
            yield self.collect_file_statistics()
        elif os.path.isdir(self.path):
            yield from self.collect_directory_statistics()
        else:
            print('The file or path specified does not exist')

    def collect_file_statistics(self) -> LogFileStats:
        log_parser = LogFileParser(path_to_file=self.path, config=self.config)
        return log_parser.process_log_file()

    def collect_directory_statistics(self) -> Iterable[LogFileStats]:
        log_parser = LogDirectoryParser(self.path, config=self.config)
        yield from log_parser.process_log_directory()


class LogDirectoryParser:
    def __init__(self, directory_path: str, config: InputSettings):
        self.directory_path = directory_path
        self.config = config

    def process_log_directory(self) -> Iterable[LogFileStats]:
        for root, dirs, files in os.walk(self.directory_path):
            for file in [file for file in files if file.lower().endswith(('.log', '.txt'))]:
                log_file_parser = LogFileParser(path_to_file=os.path.join(root, file), config=self.config)
                yield log_file_parser.process_log_file()


class LogFileParser:
    file_stats: LogFileStats
    path_to_file: str

    def __init__(self, path_to_file: str, config: InputSettings):
        self.path_to_file = path_to_file
        self.file_stats = LogFileStats(path_to_file)
        self.config = config

    def process_line(self, line: list):
        if len(line) < len(self.config.required_fields):
            return

        self.file_stats.operations += 1
        date = convert_str_to_datetime(line[self.config.required_fields.timestamp_id])
        self.file_stats.set_first_entry_time(date)
        # we can use regexp to check if IP column is correct IP
        self.file_stats.ips[line[self.config.required_fields.ip_id]] += 1
        self.file_stats.amount_of_bytes += float(line[self.config.required_fields.bytes_id])

    @timer
    def process_log_file(self) -> LogFileStats:
        with open(self.path_to_file) as log_file:
            for line in log_file:
                line = str.split(line)
                try:
                    self.process_line(line)
                except (IndexError, ValueError):
                    logger.log(level=logging.WARNING, msg="Wrong data in line")

            # I don't know if we can assume that log has correct dates.
            # If not then we should try to save every entry in order to get last valid date.
            last_time = convert_str_to_datetime(line[self.config.required_fields.timestamp_id])
            self.file_stats.last_entry_timestamp = last_time
        return self.file_stats


def get_config(cmd_arguments) -> InputSettings:
    if cmd_arguments.config \
            and os.path.isfile(cmd_arguments.config) \
            and str(cmd_arguments.config).endswith('.json'):
        path_to_config = cmd_arguments.config
    else:
        path_to_config = os.path.join(Path(__file__).parent.resolve(), "config.json")

    config = InputSettings.parse_file(path_to_config)
    return config
