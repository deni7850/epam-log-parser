from arg_parser import CommandLine
from log_parser import LogPathParser, get_config
from output_functions import save_output_to_file, StatisticsCalculator

if __name__ == '__main__':
    app = CommandLine()
    config = get_config(app.arguments)
    parser = LogPathParser(app.arguments, config)
    stats = parser.process_logs()
    for stat in stats:
        output = StatisticsCalculator(stat).calculate_statistics()
        path_to_stats = save_output_to_file(path_to_log_file=stat.file_path, output=output)
        print(path_to_stats)
