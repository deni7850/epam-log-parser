# Log parsing command line tool
Test assignment in EPAM

## To start using app run
`docker-compose run --rm app`

If you need to mount local files to image,
use example below

`docker-compose run -v /PathToLogs/logs://home/app_user/logs app`

for parsing directory 
`python3 main.py logs`

for parsing single file
`python3 main.py logs/log_file.log`