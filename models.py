import tracemalloc
from collections import defaultdict
from datetime import datetime
from functools import wraps
from time import perf_counter
from typing import List

from pydantic import BaseModel
from pydantic import Field
from pydantic import validator


def convert_str_to_datetime(timestamp: str) -> datetime | None:
    try:
        return datetime.fromtimestamp(float(timestamp))
    except (ValueError, TypeError):
        return None


def timer(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        tracemalloc.start()
        start = perf_counter()
        result = f(*args, **kwargs)
        end = perf_counter()
        print(f.__name__, end - start)
        print(tracemalloc.get_traced_memory())
        tracemalloc.stop()
        return result

    return wrapper


class LogFileStats:
    ips: defaultdict
    amount_of_bytes: int
    first_entry_timestamp: datetime | None
    last_entry_timestamp: datetime
    operations: int
    file_path: str

    def __init__(self, file_path: str):
        self.ips = defaultdict(lambda: 0)
        self.amount_of_bytes = 0
        self.operations = 0
        self.file_path = file_path
        self.first_entry_timestamp = None

    def set_first_entry_time(self, timestamp: datetime) -> None:
        if not self.first_entry_timestamp:
            self.first_entry_timestamp = timestamp


class OutputData(BaseModel):
    eps: float = Field(alias='EventsPerSecond')
    top_ip: str | None = Field(alias='MostFrequentIP')
    last_ip: str | None = Field(alias='LeastFrequentIP')
    bytes_amount: float | None = Field(alias='TotalAmountOfBytesExchanged')

    class Config:
        allow_population_by_field_name = True


class RequiredInputFields(BaseModel):
    ip_id: int
    bytes_id: int
    timestamp_id: int

    def __len__(self):
        return len(self.__fields__)


class InputSettings(BaseModel):
    required_fields: RequiredInputFields
    splitter: str
    log_file_formats: List[str]

    @validator('splitter')
    def single_character(cls, v):
        if len(v) > 1:
            raise ValueError('Splitter must be single character')
        return v
