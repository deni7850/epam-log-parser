import argparse


class CommandLine:
    def __init__(self):
        parser = argparse.ArgumentParser(description="Description for my parser")
        parser.add_argument('path', metavar='path', type=str, help='Path to log file or directory')
        parser.add_argument("-c", "--config", help="Path to config file", required=False, default="")

        self.arguments = parser.parse_args()
        argument = self.arguments

        if argument.config:
            print("You have used '-c' or '--config' with argument: {0}".format(argument.print))
