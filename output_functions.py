import os
from datetime import datetime

from models import LogFileStats, OutputData


class StatisticsCalculator:
    def __init__(self, stats: LogFileStats):
        self.stats = stats

    def _get_most_frequent_ip(self) -> str:
        try:
            return next(reversed(self.stats.ips.keys()))
        except StopIteration:
            return ''

    def _get_least_frequent_ip(self) -> str:
        try:
            return next(iter(self.stats.ips.keys()))
        except StopIteration:
            return ''

    def _get_events_per_second(self) -> float:
        if self.stats.last_entry_timestamp is not datetime or self.stats.first_entry_timestamp is not datetime:
            return 0
        time_consumed = self.stats.last_entry_timestamp - self.stats.first_entry_timestamp
        result = self.stats.operations / time_consumed.seconds
        return result

    def _get_total_amount_of_exchanged_bytes(self) -> float:
        return self.stats.amount_of_bytes

    def calculate_statistics(self) -> OutputData:
        if self.stats.ips:
            self.stats.ips = dict(sorted(self.stats.ips.items(), key=lambda item: item[1]))
            last_ip = self._get_least_frequent_ip()
            top_ip = self._get_most_frequent_ip()
        else:
            last_ip = top_ip = None
        result = OutputData(
            eps=self._get_events_per_second(),
            last_ip=last_ip,
            top_ip=top_ip,
            bytes_amount=self._get_total_amount_of_exchanged_bytes()
        )
        return result


def save_output_to_file(path_to_log_file: str, output: OutputData) -> str:
    if not os.path.isfile(path_to_log_file):
        raise ValueError("Path to file isn't a file")

    path_to_log_file = path_to_log_file + '.json'
    with open(path_to_log_file + '.json', 'w+') as file:
        file.writelines(output.json(by_alias=True))
    return path_to_log_file
